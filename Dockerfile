FROM debian:buster
RUN apt-get update && \
    apt-get install -y \
    vim \ 
    curl \
    squashfs-tools \
    apt-utils \ 
    gnupg2 \ 
    procps \
    imagemagick

# adjust sources.list:
COPY grml.list /etc/apt/sources.list.d/

#Copy other configs
COPY fai.list /etc/apt/sources.list.ld/fai.list
COPY grml-live.local /etc/grml/

# Add the grml repo key
RUN apt-key adv --keyserver hkp://pool.sks-keyservers.net:80 --recv-keys 21E0CA38EA2EA4AB 

# get keyring for apt:
RUN apt-get update 
RUN apt-get -y --allow-unauthenticated install grml-debian-keyring

# install relevant tools
RUN apt-get -y --no-install-recommends install grml-live

# get gpg key of FAI repos and install current FAI version:
RUN wget -O - http://jenkins.grml.org/debian/C525F56752D4A654.asc | apt-key add - && \
    apt-get update && \
    apt-get install -y fai-client fai-server fai-doc
