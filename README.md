# lh-linux-iso-builder

Linux iso builder docker image. 

Will create a docker image that builds a customized grml image for the ninja-recovery project.

Current deficiencies:
   Branding not working.

How to build:
# Build docker image
docker image build -t lh-iso-builder --network=host .
# Create contianer
docker create --net=host -t -i lh-iso-builder bash
# Start and connec to container
docker containter start -a -i <container-name>
# Pick a build directory
cd <target-dir>
# Ensure the output dir exists
mkdir grml
# Start iso build
grml-live -a i386

The iso file will be in grml/grml_isos